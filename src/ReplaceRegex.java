import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReplaceRegex {
    static private Path path = Paths.get("test.txt");
    static private Charset charset = StandardCharsets.UTF_8;
    static private String content;
    static private String regexFunction = "(\\\"(.+?)\\\":)(\\\"?\\$\\{__.+?\\)\\}\\\"?)";
    static private String regexInScope = "(\\\"(.+?)\\\":)\\\"([^\\d\\$].*?)\\\"";
    static private String regexJMeterVarBrackets = ":\\\"?\\$\\{([a-zA-Z].+?)\\}\\\"?";
    static private String regexJMeterVarQuotes = "\\\"(.+?)\\\"(:)";
    static private String stringDef = "def %s = vars.get(\"%s\")";
    static private String stringImport = "import groovy.json.JsonBuilder";
    static private String stringRequestBody = "def request_body = ";


    private static String name = "".replaceAll(" ", "_");


    static private String stringTextEnd = "def request_json = new JsonBuilder(request_body)\n" +
            "vars.put(\"" + name + "_json_body_request\", request_json.toPrettyString())";

    public static void replace(){

        Map<String, String> regexMap = new TreeMap();
        regexMap.put(regexJMeterVarBrackets, ":$1"); //remove brackets in jmeter variables
        regexMap.put(regexFunction, "$1$2"); //replace function by variable name
        regexMap.put(regexInScope, "$1$2"); //replace inScope by variable name
        regexMap.put(regexJMeterVarQuotes, "$1$2"); //remove quotes
        regexMap.put("\\{[^_]", "[");
        regexMap.put("[^\\)]\\}", "]");

        for (Map.Entry<String, String> entry : regexMap.entrySet()){
            String key = entry.getKey();
            String value = entry.getValue();
            content = content.replaceAll(key, value);
            System.out.println(value);
        }
        //Files.write(path, content.getBytes(charset));
    }


    private static Set<String> getAllJMeterVariableNames(){
        Set<String> varList = new HashSet<>();
        String regex = "\\\"?\\$\\{([a-zA-Z].+?)\\}\\\"?";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
            String varName = matcher.group(1);
            System.out.println("Full match: " + varName);
            varList.add(varName);
        }
        return varList;
    }

    private static Map<String, String> getAllJMeterFunctions(){
        Map functionMap = new TreeMap<String, String>();
        String regex = regexFunction;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        while(matcher.find()){
            String varName = matcher.group(2);
            String functionName = matcher.group(3);
            System.out.println("varName: " + varName);
            System.out.println("Function: " + functionName);
            functionMap.put(varName, functionName);
        }
        return functionMap;
    }

    private static Map<String, String> getAllInQuotes(){
        Map inQuotesMap = new TreeMap<String, String>();
        String regex = regexInScope;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(content);
        while(matcher.find()){
            String varName = matcher.group(2);
            String inScope = matcher.group(3);
            System.out.println("varName: " + varName);
            System.out.println("inBrackets: " + inScope);
            inQuotesMap.put(varName, inScope);
        }
        return inQuotesMap;
    }

    private static void generateNewFile(Set<String> varList,  Map<String, String> functionMap,
                                        Map<String, String> inQuotesMap) throws IOException {
        StringBuffer stringFile = new StringBuffer()
                .append(stringImport).append("\n\n");

        for (String s : varList) {
            String tmp = String.format(stringDef, s, s);
            stringFile.append(tmp).append("\n");
        }
        for (Map.Entry<String, String> entry : functionMap.entrySet()){
            String tmp = String.format("def %s = %s", entry.getKey(), entry.getValue());
            stringFile.append(tmp).append("\n");
        }for (Map.Entry<String, String> entry : inQuotesMap.entrySet()){
            String tmp = String.format("def %s = \"%s\"", entry.getKey(), entry.getValue());
            stringFile.append(tmp).append("\n");
        }

        stringFile.
                append("\n" + stringRequestBody)
                .append(content)
                .append("\n\n")
                .append(stringTextEnd);

        Files.write(path, stringFile.toString().getBytes(charset));

        System.out.println(stringFile.toString());
    }

    private static void getFileContent() throws IOException {
        content = new String(Files.readAllBytes(path), charset);
    }

    public static void main(String[] args) throws IOException {
        getFileContent();
        Set<String> varList = getAllJMeterVariableNames();
        Map<String, String> functionMap = getAllJMeterFunctions();
        Map<String, String> inQuotesMap = getAllInQuotes();
        replace();
        generateNewFile(varList, functionMap, inQuotesMap);
    }
}
